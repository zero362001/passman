Listing:

    1. Service#1
        a. username:    username
            -email_id:  email (optional)
            -password:  password
        b. username:    username#b
            -email_id:  email (optional)
            -password:  password
        .
        .
        .
        x. username:    username#x
            -email_id:  email (optional)
            -password:  password
    .
    .
    .
    n. Service#n
        a. username:    username
            -email_id:  email (optional)
             -password:  password
        b. username:    username#b
            -email_id:  email (optional)
            -password:  password
        .
        .
        .
        x. username:    username#x
            -email_id:  email (optional)
            -password:  password
---------------------------------------------------------------------------
    [n,x] CopyPass:
