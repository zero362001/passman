from setuptools import setup

with open('readme.md', 'r') as readme:
    long_description = readme.read()

setup(
    name='passman',
    version='0.1',
    description='A simple cli password manager',
    long_description=long_description,
    author='Asif Zaman',
    author_email='zero362001@protonmail.com',
    packages=['passman'],
    install_requires=['pyperclip', 'keyring', 'pycryptodome', 'argon2-cffi', 'firebase_admin'],
    scripts=['passman']
)
