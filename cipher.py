from Crypto import Random
from Crypto.Cipher import AES
from argon2 import hash_password_raw, PasswordHasher, low_level, exceptions


class Cipher(object):

    def __init__(self, password: bytes, salt: bytes):
        # self.password = password
        self.salt = salt
        self.block_size = AES.block_size
        self.key = self.kdf(password, salt)

    def encrypt(self, data: bytes) -> bytes:
        iv = Random.new().read(self.block_size)
        cipher = AES.new(self.key, AES.MODE_CBC, IV=iv)
        data = self.pad(data)
        # print(data)
        enc = cipher.encrypt(data)
        return iv + enc

    def decrypt(self, data: bytes) -> str:
        iv = data[:16]  # the IV (16 bytes)
        cipher = AES.new(self.key, AES.MODE_CBC, IV=iv)
        data = data[16:]
        dec = cipher.decrypt(data).decode()
        dec = self.unpad(dec)
        return dec

    def pad(self, data: bytes) -> bytes:
        pad_len = self.block_size - (len(data) % self.block_size)
        pad_len_str = (chr(pad_len) * pad_len).encode()
        padded = data + pad_len_str
        return padded

    @staticmethod
    def unpad(data: str) -> str:
        if len(data) == 0:
            return data
        else:
            pad_len = ord(data[-1])
            size = len(data) - pad_len
            unpadded = data[:size]
            return unpadded

    @staticmethod
    def verify_pass(hashed: str, password: bytes) -> bool:
        hasher = PasswordHasher()
        try:
            result = hasher.verify(hashed, password)
        except exceptions.VerifyMismatchError:
            return False
        return result

    @staticmethod
    def hash_pass(password: str) -> str:
        hasher = PasswordHasher()
        _hash = hasher.hash(password)
        return _hash

    @staticmethod
    def kdf(password: bytes, salt: bytes) -> bytes:
        key = hash_password_raw(time_cost=12,
                                memory_cost=2 ** 15, parallelism=2,
                                hash_len=32, password=password,
                                salt=salt, type=low_level.Type.ID)
        return key
