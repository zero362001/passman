PassMan
==========

! Not usable yet !

Passman is a simple command line password manager
that uses AES to encrypt your passwords with a key
derived from a user provided master password using the 
Argon2 Key Derivation Function.
