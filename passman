#!/bin/python3

import os
import sys
import curses
import getopt
import keyring
import pyperclip
import subprocess
from getpass import getpass
from json import dumps, loads

import mega.errors
from csvserialiser import import_db, export_db

c_war = "\033[0;33m"
c_err = "\033[0;31m"
c_scs = "\033[0;32m"
c_aca = "\033[0;34m"
c_acb = "\033[0;35m"
c_msg = "\033[3m"
s_bld = "\033[1m"
s_und = "\033[4m"
f_end = "\033[0m"

spec_char = '!@#$%^&*()_?'
data_path = '.dat'
hash_path = '.hash'
api_key_path = 'default'
bucket_name = 'default'
use_keyring = False
use_mega_sync = False
man = None
cloud_handle = None
mega_email = 'none'
mega_password = 'none'  # REMOVE!
use_same_password = False


def usage():
    doc = '''
    PassMan is a command line password manager
    that uses AES to encrypt your passwords and
    easily retrieves them straight from the terminal

    Usage:

    passman [MODE] [OPTIONS]

    ------------------------

    MODES:
        add             Add a new entry
        rem             Remove an entry
        gen             Generate a random password of given length
        lst             List entries
        get             Copy entry to clipboard
        rst             Reset master password
        exp             Export database to CSV file
        imp             Import passwords from CSV file

    ------------------------

    OPTIONS:

    MODE = add
        -s[--service=]  The name of the service
        -u[--username=] The username for the account
        -p[--password=] The password for the account; defaults to random if not passed

    MODE = rem
        -s[--service=]  The service id in the form [username@servicename]

    MODE = gen
        -l[--length=]   The length of the password to generate >= 12

    MODE = lst
        -s[--service=]  Filter by service name  *optional*
        -u[--username=] Filter by username      *optional*
        -r[--reverse]   List in reverse order   *optional*

    MODE = get
        -s[--service=]  The service id in the form [username@servicename]
        -v[--verbose]   Prints the entry to the terminal
        -u[--username]  Gets the username
            or
        -p[--password]  Gets the password *default*

    MODE = rst
        -p[--password]  New master password
    
    MODE = exp
        -f[--file=]     The file to which the database will be written *required*

    MODE = imp
        -f[--file=]     The CSV file from which the credentials are to be imported
                        The source file must have the following column headers:
                            url,  username,   password
    '''
    print(doc)


def clear_screen():
    if os.name == 'posix':
        os.system('clear')
    else:
        os.system('cls')


def get_screen_size():
    rows, columns = os.popen('stty size', 'r').read().split()
    return int(rows), int(columns)


def print_option(cmd, options, hint=''):
    cmd = f'{c_war}{s_bld}:{cmd}{f_end}'
    optstr = []
    for opt in options:
        tokens = opt.split(':')
        if len(tokens) == 1:
            optstr.append(f'{c_scs}{tokens[0]}' + f'{c_aca}{c_msg}<value>{f_end}')
        else:
            optstr.append(f'{c_scs}{tokens[0]}' + f'{c_aca}{c_msg}<value:[{tokens[1]}]>{f_end}')
    line = ('{:<25} ' + '{:<50}' * len(options)).format(cmd, *optstr)
    print(line)
    if hint:
        print(f'{c_war}{hint}{f_end}')


def get_opt_tui():
    clear_screen()
    row, col = get_screen_size()
    title = f'{c_scs}Pass{c_war}{s_bld}MAN{f_end}'
    print('\n\n' + title.center(col + 22))
    print(f'{c_war}========={f_end}'.center(col + 11) + '\n\n')
    print_option('lst', ['-[-s]ervice=:all', '-[-u]sername=:all', '-[-r]everse=:false'])
    print_option('add', ['-[-s]ervice=', '-[-u]sername=', '-[-p]assword=:random'])
    print_option('rem', ['-[-s]ervice=', '-[-u]sername='])
    print_option('gen', ['-[-l]ength=:12'])
    print_option('imp', ['-[-f]ile='])
    print_option('exp', ['-[-f]ile='])
    print_option('rst', [])
    print(f'{c_war}{s_bld}:q{c_aca}[w/!]{f_end}')  # 13 rows (Future me: ??????)
    print('\n' * (row - 16))
    print('-' * col)
    tokens = input(':').split()
    return tokens


def get_master_pass():
    return getpass('\nPassword: ')


def create_master_password():
    print('First time logging in')
    print('You need to create a master password')
    print('It must be >= 12 characters and have at least one of:')
    print('\tlowercase letters')
    print('\tuppercase letters')
    print('\t[0-9] digits')
    print('\t[{}]'.format(','.join(spec_char)))
    key = getpass('\nPassword: ')
    while not Manager.validate_key(spec_char, key):
        print(f'{c_war}Invalid key! Try again...{f_end}')
        key = getpass('\nPassword: ')
    return key


def fail_attempt():
    print(f'{c_err}Wrong password! Try again...{f_end}')


def fail_login():
    print(f'{c_err}{s_bld}Exceeded maximum number of wrong attempts!\nThe program will exit now...{f_end}')


def add_pass(entry):
    provided = True
    tried = False
    if not entry:
        entry = {}
        provided = False
        clear_screen()
        print(f'{c_msg}\n\n\nAdd new password{f_end}')
        print('\t' + ('-' * 30), '\n')
        entry['service'] = input(f'{c_msg}\tService Name: {f_end}')
        entry['uname'] = input(f'{c_msg}\tUsername/mail: {f_end}')
        entry['pword'] = ''
    if 'pword' not in entry.keys():
        entry['pword'] = Manager.generate_random(spec_char, 12)
    while not Manager.validate_key(spec_char, entry['pword']):
        if tried:
            print(f'{c_war}Password not strong enough!\n\tTry again{f_end}')
            sel = input(f'{c_war}Enter "f" to force add password{f_end}\n:')
            if sel == 'f':
                break
        if not provided:
            entry['pword'] = getpass(f'{c_msg}Password<default=random(12)>: {f_end}')
        if entry['pword'] == '':
            entry['pword'] = Manager.generate_random(spec_char, 12)
        tried = True
    sid = f"{entry['uname']}@{entry['service']}"
    return sid, entry['pword']


def clamp(x_min, x_max, x):
    if x <= x_min:
        return x_min
    elif x >= x_max:
        return x_max
    else:
        return x


def print_list(stdscr, data, lst_start=0, last=0):
    stdscr.clear()
    row_n, col_n = stdscr.getmaxyx()
    row_n -= 2
    end = (len(data) - lst_start) <= row_n
    curses.start_color()
    curses.use_default_colors()
    curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_WHITE)
    stdscr.addstr(0, 0, '{:<20} - {:<35} - {:<25}'.format(*data[0]), curses.A_STANDOUT)
    for i in range(1, min(row_n, len(data) - lst_start)):
        item = data[lst_start + i + 0]
        _str = '{:<20} - {:<35} - {:<25}'.format(item[0], item[1], item[2])
        stdscr.addstr(i, 0, _str)
    stdscr.addstr(row_n, 0, '(EOF)' if end else '...', curses.A_STANDOUT if end else curses.color_pair(0))
    stdscr.refresh()
    while True:
        key = stdscr.getch()
        if key == ord('q'):
            return 0
        elif key in (ord('j'), 258):
            if end and last != 20:
                print('\a')
            if end:
                return 20
            else:
                return 1
        elif key in (ord('k'), 259):
            return -1
        else:
            return 10


def lst_pass(stdscr, data):
    x, option = 0, 0
    while True:
        option = print_list(stdscr, data, x, option)
        if option == 0:
            return
        elif option in (1, -1):
            x = clamp(0, len(data) - 1, x + option)


def confirm_creation(sid, password):
    print('\n\tEntry created:')
    print(f'\t{c_war}{sid} -> {password}{f_end}')
    stat = input('\n\tConfirm action? (Y/N) ')
    return stat in ('y', 'Y')


def get_confirmation(msg):
    print(f'{c_war}{msg}{f_end}')
    selection = input('Are you sure you want to proceed (Y|n) ? ')
    return selection in ('Y', 'y')


def warn(msg):
    print(f'{c_war}\n{msg}{f_end}')
    input('Enter any key...')


def confirm_exit():
    stat = input(f'{c_war}\nYou are about to exit.\nAre you sure? (y/n): {f_end}')
    return stat in ('y', 'Y')


def show_cancellation(msg):
    print(f'\n\n\n{c_war}{msg}')
    print('-' * 30)
    input(f'Operation Cancelled!\nPress any key to continue...{f_end}\n')


def start_tui():
    while True:
        opts = get_opt_tui()
        if parse_options(opts, True) == -1:
            sys.exit()


def get_entry(args):
    try:
        optlist, args = getopt.getopt(args, 'vs:up',
                                      ['verbose', 'service=', 'username', 'password'])
        data = man.get_data()
        sid = ''
        uname = False
        print_to_console = False
        for opts in optlist:
            if ('--service' in opts) or ('-s' in opts):
                sid = opts[1]
            elif ('--username' in opts) or ('-u' in opts):
                uname = True
            if ('--verbose' in opts) or ('-v' in opts):
                print_to_console = True
        if not sid:
            print(f'{c_war}Expected service id in the form username@service!{f_end}')
        if uname:
            pyperclip.copy('@'.join(sid.split('@')[0:-1]))
            if print_to_console:
                print('@'.join(sid.split('@')[0:-1]))
        else:
            if man.has_entry(sid):
                if print_to_console:
                    print(data[sid])
                pyperclip.copy(data[sid])
            else:
                matches = get_matches(sid)
                if len(matches) > 0:
                    print(f"{c_war}Couldn't find entry with that id but found these matches:{f_end}")
                    for i in range(len(matches)):
                        print('{:<2} | {:<35} - {:<35}'.format(i, matches[i][0], matches[i][1]))
                    _id = input('Entry no. :')
                    if _id != 'q':
                        try:
                            _key = '@'.join(matches[int(_id)])
                            pyperclip.copy(data[_key])
                        except (ValueError, IndexError) as e:
                            print(f'{c_err}{e}{f_end}')
                else:
                    print('No such entry in database!')
    except getopt.GetoptError as err:
        print(err)
        usage()
        sys.exit()


def add_entry(args, tui=False):
    try:
        optlist, args = getopt.getopt(args, 's:u:p:',
                                      ['service=', 'username=', 'password='])
        entry = {}
        for option in optlist:
            if ('--service' in option) or ('-s' in option):
                try:
                    entry['service'] = option[1]
                except IndexError:
                    print('Invalid arguments!')
            elif ('--username' in option) or ('-u' in option):
                try:
                    entry['uname'] = option[1]
                except IndexError:
                    print('Invalid arguments!')
            elif ('--password' in option) or ('-p' in option):
                try:
                    entry['pword'] = option[1]
                except IndexError:
                    print('Invalid arguments!')
        creds = add_pass(entry)
        man.add_entry(creds)
        if not tui:
            man.save()
    except getopt.GetoptError as err:
        print(err)
        usage()
        sys.exit()


def gen_pass(args):
    try:
        optlist, args = getopt.getopt(args,
                                      'l:', ['length='])
        ln = 0
        for opts in optlist:
            if ('-l' in opts) or ('--length' in opts):
                try:
                    ln = int(opts[1])
                except ValueError as err:
                    print(err)
        pwd = man.generate_random(spec_char, ln)
        print(f'Generated password = {pwd}')
        pyperclip.copy(pwd)
    except getopt.GetoptError as err:
        print(err)
        usage()
        sys.exit()


def get_matches(sid: str):
    service_in = sid.split('@')[-1]
    data = man.get_data()
    matches = []
    for key in data:
        entry = key.split('@')
        service = entry[-1]
        u_name = '@'.join(entry[0:-1])
        if service_in in service or service in service_in:
            matches.append([u_name, service])
    return matches


def rem_entry(args, tui=False):
    try:
        optlist, args = getopt.getopt(args,
                                      's:', ['service='])
        if len(optlist) > 0:
            try:
                sid = optlist[0][1]
                if man.has_entry(sid):
                    if get_confirmation(f'You are about to remove: {sid}'):
                        man.rem_entry(sid)
                    if not tui:
                        man.save()
                    print('Removed entry...')
                else:
                    matches = get_matches(sid)
                    if len(matches) > 0:
                        print(f"{c_war}Couldn't find entry with that id but found these matches:{f_end}")
                        for i in range(len(matches)):
                            print('{:<2} | {:<35} - {:<35}'.format(i, matches[i][0], matches[i][1]))
                        _id = input('Entry no. :')
                        if _id != 'q':
                            try:
                                _key = '@'.join(matches[int(_id)])
                                if get_confirmation(f'You are about to remove: {_key}'):
                                    man.rem_entry(_key)
                            except (ValueError, IndexError) as e:
                                print(f'{c_err}{e}{f_end}')
                    else:
                        print('No such entry in database!')
            except KeyError:
                print('Invalid arguments!')
    except getopt.GetoptError as err:
        print(err)
        usage()
        sys.exit()


def lst_entries(args):
    try:
        optlist, args = getopt.getopt(args,
                                      's:u:r', ['service=', 'username=', 'reverse'])
        filter_service = ''
        filter_user = ''
        rev = False
        for opts in optlist:
            if ('-s' in opts) or ('--service' in opts):
                filter_service = opts[1]
            if ('-u' in opts) or ('--username' in opts):
                filter_user = opts[1]
            if ('-r' in opts) or ('--reverse' in opts):
                rev = True
        data = man.get_data()
        parsed = [['SERVICE', 'USERNAME', 'PASSWORD']]
        for key in data:
            entry = key.split('@')
            service = entry[-1]
            u_name = '@'.join(entry[0:-1])
            pword = data[key]
            if filter_service:
                if filter_service not in service:
                    continue
            if filter_user:
                if filter_user not in u_name:
                    continue
            parsed.append([service, u_name, pword])
        if rev:
            parsed.reverse()
        curses.wrapper(lst_pass, parsed)
    except getopt.GetoptError as err:
        print(err)
        usage()
        sys.exit()


def import_entries(args, tui=False):
    optlist, args = getopt.getopt(args[1:], 'f:', ['file='])
    data = man.get_data()
    if len(optlist) > 0:
        imported_data = import_db(optlist[0][1])
        for entry in imported_data:
            if man.has_entry(entry):
                if data[entry] != imported_data[entry]:
                    if input(f'{c_war}Entry for {entry} already exists in database. Overwrite? Y/N: {f_end}') \
                            not in ('y', 'Y'):
                        print(f'{c_msg}Skipping entry{f_end}')
                        continue
            man.add_entry([entry, imported_data[entry]])
        if not tui:
            man.save()
        print(f'{c_scs}Successfully imported entries!{f_end}')
    else:
        usage()
        sys.exit()


def export_entries(args):
    optlist, args = getopt.getopt(args[1:], 'f:', ['file='])
    data = man.get_data()
    if len(optlist) > 0:
        pword = getpass(f'{c_msg}Enter your password to confirm action: {f_end}')
        filename = optlist[0][1]
        if man.verify_creds(pword):
            if export_db(filename=filename, data=data) == 0:
                print(f'{c_scs}Export successful! Database written to {filename}{f_end}')
            else:
                print(f'{c_err}Failed to export database!')
        else:
            print(f'{c_err}Incorrect password!')
    else:
        usage()
        sys.exit()


def parse_options(args, tui=False) -> None:
    if len(args) == 0:
        start_tui()
    if args[0] in ('-h', '--help'):
        usage()
        sys.exit()
    elif args[0] == 'add':
        add_entry(args[1:], tui)
    elif args[0] == 'gen':
        gen_pass(args[1:])
    elif args[0] == 'rem':
        rem_entry(args[1:], tui)
    elif args[0] == 'lst':
        lst_entries(args[1:])
    elif args[0] == 'get':
        get_entry(args[1:])
    elif args[0] == 'imp':
        import_entries(args[1:], tui)
    elif args[0] == 'exp':
        export_entries(args[1:])
    elif args[0] == 'q':
        if confirm_exit():
            sys.exit()
    elif args[0] == 'q!':
        sys.exit()
    elif args[0] == 'qw':
        man.save()
        sys.exit()
    else:
        if len(args) > 0:
            print('invalid usage!')
            if not tui:
                usage()
                sys.exit()
        else:
            return


def set_to_keyring(key):
    sp = subprocess.Popen(['whoami'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    user = sp.stdout.read().decode('ascii')[0:-1]
    keyring.set_password('passman', user, key)
    return True


def get_from_keyring():
    sp = subprocess.Popen(['whoami'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    user = sp.stdout.read().decode('ascii')[0:-1]
    key = keyring.get_password('passman', user)
    return key


def generate_config():
    conf = {'data_path': 'default', 'hash_path': 'default', 'use_keyring': False,
            'use_mega_sync': False, 'pword_len': 12, 'spec_char': spec_char,
            'mega_email': 'none', 'use_same_password': False}
    conf_str = dumps(conf)[1:-1]
    conf_str = '{\n\t' + conf_str.replace(', ', ',\n\t') + '\n}'
    try:
        with open(os.path.expanduser('~/.config/pman.json'), 'w') as _file:
            _file.write(conf_str)
            return True
    except FileNotFoundError:
        print(f'{c_err}Failed to generate config file!{f_end}')
        return False


def load_config():
    global data_path, hash_path, use_keyring, cloud_handle
    global use_mega_sync, mega_email, mega_password, use_same_password
    config_path = os.path.expanduser('~/.config/pman.json')
    if os.path.exists(config_path):
        with open(config_path, 'r') as config_file:
            conf = loads(config_file.read())
            if conf['data_path'] != 'default':
                data_path = conf['data_path']
            if conf['hash_path'] != 'default':
                hash_path = conf['hash_path']
            use_keyring = conf['use_keyring']
            use_mega_sync = conf['use_mega_sync']
            use_same_password = conf['use_same_password']
            mega_email = conf['mega_email'] 
            mega_password = conf['mega_password']  # REMOVE!
        return True
    else:
        return generate_config()


def update_config(key, value):
    config_path = os.path.expanduser('~/.config/pman.json')
    if os.path.exists(config_path):
        with open(config_path, 'r+') as config_file:
            conf = loads(config_file.read())
            conf[key] = value
            config_file.seek(0)
            config_file.write(dumps(conf))
            config_file.truncate()
        return True
    else:
        return False


def attempt_login(count):
    keyring_update_required = False
    if not os.path.exists(hash_path):
        key = create_master_password()
        man.reset_master_password(key)
        if use_keyring:
            set_to_keyring(key)
        return key
    key = ''
    if use_keyring:
        key = get_from_keyring()
    if not key:
        key = get_master_pass()
        keyring_update_required = True
    code = man.verify_creds(key)
    if code == 1:
        if keyring_update_required:
            set_to_keyring(key)
        return key
    else:
        if count < 2:
            fail_attempt()
            return attempt_login(count + 1)
        else:
            fail_login()
            return False


def get_mega_creds():
    uname = input('Mega email: ') if mega_email == 'none' else mega_email
    pword = getpass('Mega password: ') if mega_password == 'none' else mega_password
    return uname, pword


def mega_login():
    global cloud_handle
    cloud_handle = MegaCloudSync()
    while True:
        mega_uname, mega_pass = get_mega_creds()
        try:
            cloud_handle.login(mega_uname, mega_pass)
            break
        except mega.errors.RequestError:
            warn('Wrong credentials!')


if __name__ == '__main__':
    load_config()
    if len(sys.argv) > 1:
        if sys.argv[1] in ('-h', 'help', '--help'):
            usage()
            sys.exit()

    cloud_handle = None
    if use_mega_sync:
        from megasync import MegaCloudSync
        mega_login()

    from manager import Manager

    man = Manager(hash_path, data_path, cloud_handle)
    master_key = attempt_login(0)
    if master_key:
        man.load()
        parse_options(sys.argv[1:])
    else:
        sys.exit()
