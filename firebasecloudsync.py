from firebase_admin import credentials, initialize_app, storage
from os import path


class CloudSync(object):

    def __init__(self, key_path, bucket_name):
        self.creds = credentials.Certificate(key_path)
        self.app = initialize_app(self.creds, {'storageBucket': bucket_name})
        self.bucket = storage.bucket()

    def save(self, file_path_local, time_modified):
        blob = self.bucket.blob(path.basename(file_path_local))
        blob.upload_from_filename(file_path_local)
        blob = self.bucket.blob('time_modified')
        blob.upload_from_string(str(time_modified))
        return True

    def is_later(self, time_modified: int):
        if not time_modified:
            return 2
        try:
            cloud_time = int(self.load('time_modified'))
            if cloud_time > int(time_modified):
                return 2
            elif cloud_time < int(time_modified):
                return 1
            else:
                return 0
        except ValueError as e:
            print('Data not found in google cloud. Using local data.')
            return -1

    def load(self, file_path):
        blob = self.bucket.blob(file_path)
        file = blob.open()
        text = file.read()
        file.close()
        return text
