import tempfile
from mega import Mega
from os import path


class MegaCloudSync(object):

    def __init__(self):
        self.mega_instance = Mega()
        self.mega_handle = None

    def login(self, uname: str, password: str):
        self.mega_handle = self.mega_instance.login(uname, password)

    def save(self, data_path: str, time_modified='') -> int:
        print('Saving...')
        """
        Saves data to cloud

        :param data_path: The path at which the data is located
        :param time_modified: The time of last modification in the format of seconds since epoch
        :return: 2 if first time uploading to server
        """
        status = 2  # First time pushing to server
        file_query = self.mega_handle.find(path.basename(data_path))
        if file_query:
            file_handle = file_query[1]['h']
            status = 1  # Since data already exists, this can't be the first push
            self.mega_handle.delete(file_handle)
        self.mega_handle.upload(data_path)
        if time_modified != '':
            file_query_modified = self.mega_handle.find('time_modified')
            if file_query_modified:
                file_handle_modified = file_query_modified[1]['h']
                self.mega_handle.delete(file_handle_modified)
            self.mega_handle.upload(time_modified, dest_filename='time_modified')
        return status

    def load(self, filename: str, dest_path: str) -> str:
        data = self.mega_handle.find(filename)
        return self.mega_handle.download(data, dest_filename=dest_path)

    def is_later(self, time_modified: int) -> int:
        data = self.mega_handle.find('time_modified')
        if not data:
            return -1  # doesn't exist
        temp = tempfile.NamedTemporaryFile(mode='w+', prefix='pman_', delete=False)
        temp.close()
        # temp = open(path.expanduser('~/tmp'), 'w+')
        self.mega_handle.download(data, dest_filename=temp.name)
        temp = open(temp.name, 'r')
        temp.seek(0)
        try:
            last_mod = int(temp.read())
            temp.close()
            if last_mod > int(time_modified):
                return 2  # more recent
            elif last_mod < int(time_modified):
                return 1  # outdated
            else:
                return 0  # same version
        except ValueError as e:
            temp.close()
            print(e)
